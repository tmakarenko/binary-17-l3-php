UPDATE users
SET users.is_deleted = TRUE
WHERE users.id NOT IN (SELECT bookings.user_id FROM bookings)