SELECT users.id,users.first_name,users.last_name,users.age FROM users
	INNER JOIN bookings ON users.id=bookings.user_id
    INNER JOIN tickets ON tickets.id=bookings.ticket_id
GROUP BY users.id,users.age
HAVING SUM(tickets.price) > 400 and users.age > 25